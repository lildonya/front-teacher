

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement


import java.io.IOException

abstract class CalculatorAbstractTest : AbstractTest<CalculatorApplication>() {
    protected lateinit var calculatorScreen: CalculatorScreen

    override fun onStart(application: CalculatorApplication) {
        calculatorScreen = application.calculatorScreen()
    }

    override fun create(driver: AppiumDriver<*>): CalculatorApplication {
        return CalculatorApplication(driver)
    }

    @Throws(IOException::class)
    override fun createAndroidDriverBuilder(): AppiumDriverBuilder.AndroidDriverBuilder {
        return super.createAndroidDriverBuilder()
                .withPackageName(constants.packageName)
                .withLaunchActivity(constants.launchActivity)
    }
}
