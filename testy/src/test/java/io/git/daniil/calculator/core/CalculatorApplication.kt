

import io.appium.java_client.AppiumDriver
import io.appium.java_client.MobileElement


class CalculatorApplication(driver: AppiumDriver<*>) : AbstractApplication(driver) {

    fun calculatorScreen(): CalculatorScreen {
        return CalculatorScreen(driver)
    }
}
