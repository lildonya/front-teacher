

import org.testng.annotations.AfterMethod

abstract class AbstractOperationTest: CalculatorAbstractTest() {
    @AfterMethod
    fun `clear`() {
        calculatorScreen.clearBtn.shouldClick()
    }
}